FROM shashwot/alpine-java:200912-4beb7ed
ARG VARIABLE

ENV VARIABLE=$VARIABLE

COPY [ "target/assignment-$VARIABLE.jar", "/opt" ]

WORKDIR "/opt/"

CMD [ "java","-jar", "assignment-$VARIABLE.jar" ]
